package com.projetoeventos.backendeventos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendEventosApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendEventosApplication.class, args);
	}
}