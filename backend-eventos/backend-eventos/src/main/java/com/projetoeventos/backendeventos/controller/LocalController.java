package com.projetoeventos.backendeventos.controller;

import com.projetoeventos.backendeventos.model.Local;
import com.projetoeventos.backendeventos.repository.LocalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class LocalController {

    @Autowired
    private LocalRepository lr;

    @RequestMapping(value = "/salvaLocal", method = RequestMethod.POST)
    public void salvaLocal(@RequestBody Local local) {
        lr.save(local);
    }

    @RequestMapping(value = "/editaLocal", method = RequestMethod.PUT)
    public void editaLocal(@RequestBody Local local) {
        lr.save(local);
    }

    @RequestMapping(value = "/deletaLocal", method = RequestMethod.DELETE)
    public void deletaLocal(@RequestParam Long id) {
        lr.deleteById(id);
    }

    @RequestMapping(value = "/locais", method = RequestMethod.GET)
    public Iterable<Local> getLocais() {
        return lr.findAll();
    }

    @RequestMapping(value = "/local", method = RequestMethod.GET)
    public Optional<Local> getLocalById(@RequestParam Long id) {
        return lr.findById(id);
    }
}