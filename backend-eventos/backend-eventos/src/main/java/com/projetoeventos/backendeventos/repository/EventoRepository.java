package com.projetoeventos.backendeventos.repository;

import com.projetoeventos.backendeventos.model.Evento;
import org.springframework.data.repository.CrudRepository;

public interface EventoRepository extends CrudRepository<Evento, Long> {
}