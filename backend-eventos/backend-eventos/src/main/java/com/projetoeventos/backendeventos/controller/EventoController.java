package com.projetoeventos.backendeventos.controller;

import com.projetoeventos.backendeventos.model.Evento;
import com.projetoeventos.backendeventos.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
public class EventoController {

    @Autowired
    private EventoRepository er;

    @RequestMapping(value = "/cadastrarEvento", method = RequestMethod.POST)
    public void salvaEvento(@RequestBody Evento evento) {
        er.save(evento);
    }

    @RequestMapping(value = "/editaEvento", method = RequestMethod.PUT)
    public void editaEvento(@RequestBody Evento evento) {
        er.save(evento);
    }

    @RequestMapping(value = "/listaEventos", method = RequestMethod.GET)
    public Iterable<Evento> listaEventos() {
        return er.findAll();
    }

    @RequestMapping(value = "/evento", method = RequestMethod.GET)
    public Optional<Evento> getEventoPeloId(@RequestParam Long id) {
        return er.findById(id);
    }

    @RequestMapping(value = "/deleteEvento", method = RequestMethod.DELETE)
    public void deleteEvento(@RequestParam Long id) {
        er.deleteById(id);
    }

    @RequestMapping(value = "/eventosPesquisa", method = RequestMethod.GET)
    public Iterable<Evento> retornaPesquisaEventos(@RequestParam String nome) {
        List<Evento> eventos = new ArrayList<>();
        for (Evento eventoFor : er.findAll()) {
            if (eventoFor.getNome().contains(nome)) {
                eventos.add(eventoFor);
            }
        }
        return eventos;
    }
}