package com.projetoeventos.backendeventos.repository;

import com.projetoeventos.backendeventos.model.Local;
import org.springframework.data.repository.CrudRepository;

public interface LocalRepository extends CrudRepository<Local, Long> {
}