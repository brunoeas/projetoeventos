import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-formulario',
  templateUrl: './template-formulario.component.html',
  styleUrls: ['./template-formulario.component.css']
})
export class TemplateFormularioComponent implements OnInit {
  @Input() titulo: string;
  @Input() nome: string;

  constructor() { }

  ngOnInit() {
  }

}
