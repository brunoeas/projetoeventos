import { Component, OnInit } from '@angular/core';
import { Local } from '../model/LocalModel';
import { LocalService } from '../local.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-locais',
  templateUrl: './lista-locais.component.html',
  styleUrls: ['./lista-locais.component.css']
})
export class ListaLocaisComponent implements OnInit {
  locais: Local[];

  constructor(private service: LocalService, private router: Router) { }

  ngOnInit() {
    this.getLocais();
  }

  getLocais() {
    this.service.getAllLocais().subscribe(locais => this.locais = locais);
  }

  deleteLocal(id: number) {
    this.locais = this.locais.filter(l => l.id_local !== id);
    this.service.deletaLocal(id).subscribe();
  }

  redirecionaParaEditar(id: number) {
    this.router.navigate([`/local/${id}`]);
  }
}