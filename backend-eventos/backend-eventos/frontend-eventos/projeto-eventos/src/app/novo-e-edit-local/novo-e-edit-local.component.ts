import { Component, OnInit } from '@angular/core';
import { Local } from '../model/LocalModel';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { LocalService } from '../local.service';

@Component({
  selector: 'app-novo-e-edit-local',
  templateUrl: './novo-e-edit-local.component.html',
  styleUrls: ['./novo-e-edit-local.component.css']
})
export class NovoEEditLocalComponent implements OnInit {
  titulo: string;
  local: Local;
  private id: number;

  constructor(private route: ActivatedRoute, private location: Location, private service: LocalService) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');

    if (this.id) {
      this.service.getLocalById(this.id).subscribe(local => this.local = local);
      this.titulo = "Editar Local";
    }else {
      this.local = new Local();
      this.local.nome = "";
      this.titulo = "Cadastrar Local";
    }
  }

  enviar() {
    if (this.id) {
      this.service.editaLocal(this.local).subscribe(() => this.goBack());
    } else {
      this.service.addLocal(this.local).subscribe(() => this.goBack());
    }
  }

  goBack() {
    this.location.back();
  }
}