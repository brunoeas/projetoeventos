import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoEEditLocalComponent } from './novo-e-edit-local.component';

describe('NovoEEditLocalComponent', () => {
  let component: NovoEEditLocalComponent;
  let fixture: ComponentFixture<NovoEEditLocalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoEEditLocalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoEEditLocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
