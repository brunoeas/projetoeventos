import { Local } from "./LocalModel";

export class Evento {
    id_evento: number;
    nome: string;
    local: Local = new Local();
    data: string;
    horario: string;
}