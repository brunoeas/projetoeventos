import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NovoEEditEventoComponent } from './novo-e-edit-evento/novo-e-edit-evento.component';
import { ListaEventosComponent } from './lista-eventos/lista-eventos.component';
import { HomeComponent } from './home/home.component';
import { ListaLocaisComponent } from './lista-locais/lista-locais.component';
import { NovoEEditLocalComponent } from './novo-e-edit-local/novo-e-edit-local.component';

const routes: Routes = [
  { path: 'evento', component: NovoEEditEventoComponent },
  { path: 'evento/:id', component: NovoEEditEventoComponent },
  { path: 'listaEventos', component: ListaEventosComponent },
  { path: 'local/:id', component: NovoEEditLocalComponent },
  { path: 'local', component: NovoEEditLocalComponent },
  { path: 'listaLocais', component: ListaLocaisComponent },
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
];

export const RoutingModule = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RotasModule { }
