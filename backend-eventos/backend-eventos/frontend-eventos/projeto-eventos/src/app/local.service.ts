import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { API } from './API';
import { Local } from './model/LocalModel';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  constructor(private http: HttpClient) { }

  addLocal(local: Local): Observable<void> {
    return this.http.post<void>(`${API}/salvaLocal`, JSON.stringify(local), httpOptions);
  }

  editaLocal(local: Local): Observable<void> {
    return this.http.put<void>(`${API}/editaLocal`, JSON.stringify(local), httpOptions);
  }

  deletaLocal(id: number): Observable<void> {
    return this.http.delete<void>(`${API}/deletaLocal?id=${id}`, httpOptions);
  }

  getAllLocais(): Observable<Local[]> {
    return this.http.get<Local[]>(`${API}/locais`);
  }

  getLocalById(id: number): Observable<Local> {
    return this.http.get<Local>(`${API}/local?id=${id}`);
  }
}