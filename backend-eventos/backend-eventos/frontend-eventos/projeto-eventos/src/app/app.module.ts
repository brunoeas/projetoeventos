import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { AppComponent } from './app.component';
import { CabecalhoComponent } from './cabecalho/cabecalho.component';
import { RodapeComponent } from './rodape/rodape.component';
import { ListaEventosComponent } from './lista-eventos/lista-eventos.component';
import { NovoEEditEventoComponent } from './novo-e-edit-evento/novo-e-edit-evento.component';
import { RotasModule } from './/rotas.module';
import { HomeComponent } from './home/home.component';
import { NovoEEditLocalComponent } from './novo-e-edit-local/novo-e-edit-local.component';
import { ListaLocaisComponent } from './lista-locais/lista-locais.component';
import { TemplateFormularioComponent } from './template-formulario/template-formulario.component';
import { TemplateListaComponent } from './template-lista/template-lista.component';
import { CampoPesquisaComponent } from './campo-pesquisa/campo-pesquisa.component';

@NgModule({
  declarations: [
    AppComponent,
    CabecalhoComponent,
    RodapeComponent,
    ListaEventosComponent,
    NovoEEditEventoComponent,
    HomeComponent,
    NovoEEditLocalComponent,
    ListaLocaisComponent,
    TemplateFormularioComponent,
    TemplateListaComponent,
    CampoPesquisaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RotasModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
