import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { API } from './API';
import { Evento } from './model/EventoModel';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  constructor(private http: HttpClient) { }

  addEvento(evento: Evento): Observable<void> {
    return this.http.post<void>(`${API}/cadastrarEvento`, JSON.stringify(evento), httpOptions);
  }

  editEvento(evento: Evento): Observable<void> {
    return this.http.put<void>(`${API}/editaEvento`, JSON.stringify(evento), httpOptions);
  }

  deleteEvento(id: number): Observable<void> {
    return this.http.delete<void>(`${API}/deleteEvento?id=${id}`, httpOptions);
  }

  getAllEventos(): Observable<Evento[]> {
    return this.http.get<Evento[]>(`${API}/listaEventos`);
  }

  getEventoById(id: number): Observable<Evento> {
    return this.http.get<Evento>(`${API}/evento?id=${id}`);
  }

  getEventosPesquisa(txt: string): Observable<Evento[]> {
    if (!txt.trim()) {
      return of([]);
    }
    return this.http.get<Evento[]>(`${API}/eventosPesquisa?nome=${txt}`);
  }
}