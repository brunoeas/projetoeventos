import { Component, OnInit } from '@angular/core';
import { Evento } from '../model/EventoModel';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { EventoService } from '../evento.service';
import { Local } from '../model/LocalModel';
import { LocalService } from '../local.service';

@Component({
  selector: 'app-novo-e-edit-evento',
  templateUrl: './novo-e-edit-evento.component.html',
  styleUrls: ['./novo-e-edit-evento.component.css']
})
export class NovoEEditEventoComponent implements OnInit {
  titulo = "";
  evento: any = {};
  private id: number;
  locais: Local[];

  constructor(private route: ActivatedRoute, private location: Location, private service: EventoService, private serviceLocal: LocalService) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');

    this.serviceLocal.getAllLocais().subscribe(locais => this.locais = locais);
    
    if (this.id) {
      this.service.getEventoById(this.id).subscribe(evento => this.evento = evento);
      this.titulo = "Editar Evento";
    }else {
      this.evento = new Evento();
      this.titulo = "Novo Evento";
    }
  }

  enviar() {
    if (this.evento.local.id_local) {
      if (this.id) {
        this.service.editEvento(this.evento).subscribe(() => this.goBack());
      }else {
        this.service.addEvento(this.evento).subscribe(() => this.goBack());
      }
    }
  }

  goBack() {
    this.location.back();
  }
}