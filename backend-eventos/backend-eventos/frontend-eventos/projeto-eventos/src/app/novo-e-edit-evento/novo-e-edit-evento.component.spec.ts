import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoEEditEventoComponent } from './novo-e-edit-evento.component';

describe('NovoEEditEventoComponent', () => {
  let component: NovoEEditEventoComponent;
  let fixture: ComponentFixture<NovoEEditEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoEEditEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoEEditEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
