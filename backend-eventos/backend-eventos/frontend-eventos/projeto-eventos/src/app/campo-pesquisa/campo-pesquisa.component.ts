import { Component, OnInit } from '@angular/core';
import { EventoService } from '../evento.service';
import { Subject, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Evento } from '../model/EventoModel';

@Component({
  selector: 'app-campo-pesquisa',
  templateUrl: './campo-pesquisa.component.html',
  styleUrls: ['./campo-pesquisa.component.css']
})
export class CampoPesquisaComponent implements OnInit {
  eventos$: Observable<Evento[]>;
  private pesquisaTerms = new Subject<string>();

  constructor(private service: EventoService) { }

  ngOnInit() {
    this.eventos$ = this.pesquisaTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((txt: string) => this.service.getEventosPesquisa(txt))
    );
  }

  pesquisar(txt: string) {
    this.pesquisaTerms.next(txt);
  }
}