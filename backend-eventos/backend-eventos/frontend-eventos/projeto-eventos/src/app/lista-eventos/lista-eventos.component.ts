import { Component, OnInit } from '@angular/core';
import { Evento } from '../model/EventoModel';
import { EventoService } from '../evento.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-eventos',
  templateUrl: './lista-eventos.component.html',
  styleUrls: ['./lista-eventos.component.css']
})
export class ListaEventosComponent implements OnInit {
  eventos: Evento[];

  constructor(private service: EventoService, private router: Router) { }

  ngOnInit() {
    this.getEventos();
  }

  getEventos() {
    this.service.getAllEventos().subscribe(eventos => this.eventos = eventos);
  }

  deleteEvento(id: number) {
    this.eventos = this.eventos.filter(e => e.id_evento !== id);
    this.service.deleteEvento(id).subscribe();
  }

  redirecionaParaEditar(id: number) {
    this.router.navigate([`/evento/${id}`]);
  }
}