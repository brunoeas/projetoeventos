import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateListaComponent } from './template-lista.component';

describe('TemplateListaComponent', () => {
  let component: TemplateListaComponent;
  let fixture: ComponentFixture<TemplateListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
