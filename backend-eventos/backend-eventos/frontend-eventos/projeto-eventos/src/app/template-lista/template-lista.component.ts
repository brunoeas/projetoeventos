import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-lista',
  templateUrl: './template-lista.component.html',
  styleUrls: ['./template-lista.component.css']
})
export class TemplateListaComponent implements OnInit {
  @Input() titulo: string;
  @Input() subtitulo: string;

  constructor() { }

  ngOnInit() {
  }

}
